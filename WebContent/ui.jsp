<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Penerimaan UI - Buat Account Baru</title>
</head>
<body>
		<h3>Buat Account Baru</h3>
		<hr>
		For international applicants, please contact our <a href="https://penerimaan.ui.ac.id/page/foreign">International Office</a> regarding application procedure.
		This registration form is meant for indonesia citizen only.<br>
		Harap menggunakan <i>account</i> lama Anda jika Anda telah membuat pendaftaran sebelumnya

	<!-- <form action="#"> -->
		<h4>Login</h4>
		<hr>
		
			<table>
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size: 12px; display: none;" id="td_username">Field ini harus diisi</td>
				</tr>
				<tr>
					<td> Username </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="text" id="username">
					
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td style="color: gray; font-size: 12px;">Username yang anda inginkan (hanya dapat terdiri dari huruf, angka dan _). 
					Username tidak boleh mengandung spasi  </td>
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size: 12px; display: none;" id="td_password">Field ini harus diisi</td>
				</tr>
				<tr>
					<td> Password </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="password" id="password">
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td style="color: gray; font-size: 12px;"> Masukkan password. Password minimal terdiri dari 6 karakter dan case sensitive (A berbeda dengan a) </td>
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size: 12px; display: none;" id="td_repassword">Password yang anda Masukkan tidak sama</td>
				</tr>
				<tr>
					<td>Ulangi Password </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="password" id="repassword" onchange="cekSama();">
				</tr>
			</table>
		
		
		
		<h4>Identitas</h4>
		<hr>
			<table>
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_name">Nama sesuai identitas harus diisi</td>
				</tr>
				<tr>
					<td> Nama Sesuai Identitas </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="text" id="name">
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td style="color: gray; font-size: 12px;">Nama sesuai yang tertulis di kartu identitas, tanpa gelar akademis/sertifikasi <br>
					Nama diawali huruf besar diawal dan setelah spasi, contoh benar: Abc Def, contoh salah: abc def, ABC DEF</td>
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_name2">Nama sesuai ijazah harus diisi</td>
				</tr>
				<tr>
					<td> Nama Sesuai Ijazah </td>
					<td><a style="color: red; ">*</a></td>
					<td><input type="text" id="name2">
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td style="color: gray; font-size: 12px;">Nama sesuai yang tertulis di kartu identitas, tanpa gelar akademis/sertifikasi <br>
					Nama diawali huruf besar diawal dan setelah spasi, contoh benar: Abc Def, contoh salah: abc def, ABC DEF</td>
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_identity">Field harus diisi</td>
				</tr>
				<tr>
					<td> Jenis Identitas </td>
					<td><a style="color: red;">*</a></td>
					<td>
						<select id="identity">
							<option value ="-">--- Pilih ---</option>
							<option value="KTP"> KTP </option>
							<option value="SIM"> SIM </option>
							<option value="Paspor"> Paspor </option>
							<option value="Kartu Pelajar"> Kartu Pelajar (Berfoto) </option>
						</select>
					</td>
				</tr>
				
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_noId">Field harus diisi</td>
				</tr>
				<tr>
					<td> Nomor Identitas </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="text" id="noId">
				</tr>
				
				
				<tr>
					<td> Kewarganegaraan </td>
					<td><a style="color: red;">*</a></td>
					<td>
						<select id="kwn">
							<option value="Indonesia" > Indonesia</option>
						</select>
					</td>
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_jk">Jenis Kelamin harus diisi</td>
				</tr>
				<tr>
					<td> Jenis Kelamin </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="radio" name="gender" id="jkWanita" value="Wanita">Wanita
						<input type="radio" name="gender" id="jkPria" value="Pria">Pria </td>
				</tr>
				<tr>
					<td> Tanggal lahir </td>
					<td><a style="color: red;">*</a></td>
					<td>
						<select id="date">
							<option value="01"> 01 </option> <option value="02"> 02 </option> <option value="03"> 03 </option> <option value="04"> 04 </option> <option value="05"> 05 </option>
							<option value="06"> 06 </option> <option value="07"> 07 </option> <option value="08"> 08 </option> <option value="09"> 09 </option> <option value="10"> 10 </option>
							<option value="11"> 11 </option> <option value="12"> 12 </option> <option value="13"> 13 </option> <option value="14"> 14 </option> <option value="15"> 15 </option>
							<option value="16"> 16 </option> <option value="17"> 17 </option> <option value="18"> 18 </option> <option value="19"> 19 </option> <option value="20"> 20 </option>
							<option value="21"> 21 </option> <option value="22"> 22 </option> <option value="23"> 23 </option> <option value="24"> 24 </option> <option value="25"> 25 </option>
							<option value="26"> 26 </option> <option value="27"> 27 </option> <option value="28"> 28 </option> <option value="29"> 29 </option> <option value="30"> 30 </option>
							<option value="31"> 31 </option>
						</select>
						<select id="month">
							<option value="Januari" > Jan </option> <option value="Februari" > Feb </option>
							<option value="Maret"> Mar </option> <option value="April" > Apr </option>
							<option value="Mei"> May </option> <option value="Juni"> Jun </option>
							<option value="Juli"> Jul </option> <option value="Agustus"> Aug </option>
							<option value="September"> Sep </option> <option value="Oktober"> Oct </option> 
							<option value="November"> Nov </option> <option value="Desember"> Dec </option>
						</select>
						<select id="year">
							<option > 1990 </option> <option > 1991 </option>
							<option > 1992 </option> <option > 1993 </option>
							<option > 1995 </option> <option > 1996 </option>
							<option > 1997 </option> <option > 1998 </option>
							<option > 1999 	</option> <option > 2000 </option> 
							<option > 2001 </option> <option > 2002 </option>
						</select>
					</td>
				</tr>
				
			</table>
			
			
			
		<h4>Kontak</h4>
		<hr>
			<table>
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_alamat">Field ini harus diisi</td>
				</tr>
				<tr>
					<td> Alamat Tetap </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="text" size="50" id="alamat">
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_region">Field ini harus diisi</td>
				</tr>
				<tr>
					<td> Negara </td>
					<td><a style="color: red;">*</a></td>
					<td>
						<select id="region" onclick="addPropinsi();">
							<option value="-"> --- Pilih --- </option>
							<option value="Amerika"> Amerika </option>
							<option value="China"> China </option>
							<option value="Korea Selatan"> Korea Selatan </option>
							<option value="Jepang"> Jepang </option>
							<option value="Indonesia"> Indonesia </option>
							<option value="Malaysia"> Malaysia </option>
						</select>
					</td>
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size: 12px; display: none;" id="td_propinsi">Field ini Harus Diisi</td>
				</tr>
				<tr>
					<td> Propinsi </td>
					<td><a style="color: red;">*</a></td>
					<td>
						<select id="propinsi" onclick="addKota();">
							<option value="-"> --- Pilih --- </option>
						</select>
					</td>
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size: 12px; display: none;" id="td_kota">Field ini Harus Diisi</td>
				</tr>
				<tr>
					<td> Kabupaten/Kotamadya </td>
					<td><a style="color: red;">*</a></td>
					<td>
						<select style="display: none;" id="td_Jabar">
							<option > --- Pilih --- </option>
							<option > Bogor </option>
							<option > Depok </option>
							<option > Citayam </option>
						</select>
						<select style="display: none;" id="td_Jateng">
							<option > --- Pilih --- </option>
							<option > Semarang </option>
							<option > Purwokerto </option>
							<option > Banjar Negara </option>
						</select>
						<select style="display: none" id="td_Jatim">
							<option > --- Pilih --- </option>
							<option > Malang </option>
							<option > Banyuwangi </option>
							<option > Bangkalan </option>
						</select>
					</td>
				</tr>
				<tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_alamatskrg">Field ini harus diisi</td>
				</tr>
				<tr>
					<td> Alamat Saat Ini </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="text" size="50" id="alamatskrg">
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_noTelp">Field ini harus diisi</td>
				</tr>
				<tr>
					<td> No. telepon </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="text" id="noTelp">
				</tr>
				
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_noHp">Field ini harus diisi</td>
				</tr>
				<tr>
					<td> No. HP </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="text" id="noHp">
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_email">Field ini harus diisi</td>
				</tr>
				<tr>
					<td> Email </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="text" id="email">
				</tr>
				
				<tr>
					<td></td>
					<td></td>
					<td style="color: red; font-size:12px; display: none;" id="td_reemail">Field ini harus diisi</td>
				</tr>
				<tr>
					<td> Ulangi email </td>
					<td><a style="color: red;">*</a></td>
					<td><input type="text" id="reemail">
				</tr>
				
				
				<tr>
					<td>  </td>
					<td>  </td>
					<td> <button type="button" onclick="cekIsi();"> Simpan</button> </td>
				</tr>
			</table>
</body>
<script type="text/javascript">
function cekIsi() {
	var username= document.getElementById("username");
	var td_username= document.getElementById("td_username");
	if (username.value=="") {
		td_username.style.display="block";
		<!-- username.style.borderColor ="red"; --->
	}else {
		td_username.style.display="none";
		username.style.borderColor ="gray";
	}
	
	var password= document.getElementById("password");
	var td_password= document.getElementById("td_password");
	if (password.value=="") {
		td_password.style.display="block";
	}else {
		td_password.style.display="none";
		password.style.borderColor ="gray";
	}
	
	var name= document.getElementById("name");
	var td_name= document.getElementById("td_name");
	if (name.value=="") {
		td_name.style.display="block";
	}else {
		td_name.style.display="none";
		name.style.borderColor ="gray";	
		
	}
	
	var name2 = document.getElementById("name2");
	var td_name2= document.getElementById("td_name2");
	if (name2.value=="") {
		td_name2.style.display="block";
	} else {
		td_name2.style.display="none";
		name2.style.borderColor ="gray";
	}
	
	var identity = document.getElementById("identity");
	var td_identity= document.getElementById("td_identity");
	if (identity.value=="-") {
		td_identity.style.display="block";
	}else {
		td_identity.style.display="none";
		identity.style.borderColor ="gray";	
		
	}
	
	var noId= document.getElementById("noId");
	var td_noId= document.getElementById("td_noId");
	if (noId.value=="") {
		td_noId.style.display="block";
	}else {
		td_noId.style.display="none";
		noId.style.borderColor ="gray";		
	}
	
	var jkWanita = document.getElementById("jkWanita");
	var jkPria = document.getElementById("jkPria")
	var td_jk= document.getElementById("td_jk");
	if (jkWanita.value == "" && jkPria.value== "") {
		td_jk.style.display="block";
	}else {
		td_jk.style.display="none";
		
	}
	
	var alamat= document.getElementById("alamat");
	var td_alamat= document.getElementById("td_alamat");
	if (alamat.value=="") {
		td_alamat.style.display="block";
		
	}else {
		td_alamat.style.display="none";
		alamat.style.borderColor ="gray";	
	}
	
	var region= document.getElementById("region");
	var td_region = document.getElementById("td_region");
	if(region.value=="-"){
		td_region.style.display="block";
	}else{
		td_region.style.display="none";
	}

	var propinsi= document.getElementById("propinsi");
	var td_propinsi = document.getElementById("td_propinsi");
	if(propinsi.value=="-"){
		td_propinsi.style.display="block";
	}else{
		td_propinsi.style.display="none";
	}
	
	var td_Jabar= document.getElementById("td_Jabar");
	var td_Jateng= document.getElementById("td_Jateng");
	var td_Jatim= document.getElementById("td_Jatim");
	var td_kota = document.getElementById("td_kota");
	if(td_Jabar.value=="-" || td_Jateng=="-" || td_Jatim=="-"){
		td_kota.style.display="block";
	}else{
		td_kota.style.display="none";
	}
	
	var alamatskrg= document.getElementById("alamatskrg");
	var td_alamatskrg = document.getElementById("td_alamatskrg");
	if(alamatskrg.value==""){
		td_alamatskrg.style.display="block";
	}else{
		td_alamatskrg.style.display="none";
	}
	
	var noTelp= document.getElementById("noTelp");
	var td_noTelp = document.getElementById("td_noTelp");
	if(noTelp.value==""){
		td_noTelp.style.display="block";
	}else{
		td_noTelp.style.display="none";
	}
	
	var noHp= document.getElementById("noHp");
	var td_noHp = document.getElementById("td_noHp");
	if(noHp.value==""){
		td_noHp.style.display="block";
	}else{
		td_noHp.style.display="none";
	}
	
	var email= document.getElementById("email");
	var td_email = document.getElementById("td_email");
	if(email.value==""){
		td_email.style.display="block";
	}else{
		td_email.style.display="none";
	}
	
	var reemail= document.getElementById("reemail");
	var td_reemail = document.getElementById("td_reemail");
	if(reemail.value==""){
		td_reemail.style.display="block";
	}else{
		td_reemail.style.display="none";
	}
}


function cekSama() {
	var repassword = document.getElementById("repassword");
	var td_repassword= document.getElementById("td_repassword");
	if (repassword.value!=password.value) {
		td_repassword.style.display="block";
	} else {
		td_repassword.style.display="none";
		repassword.style.borderColor ="gray";
	}
	
}

function addPropinsi(){
	var region= document.getElementById("region");
	var propinsi = document.getElementById("propinsi");
	
	var option1 = document.createElement("option");
	var option2 = document.createElement("option");
	var option3 = document.createElement("option");
	var option4 = document.createElement("option");
	var option5 = document.createElement("option");
	
	if(region.value=="Indonesia"){
		option1.text = " Jawa Barat ";
		option2.text = " Jawa Tengah ";
		option3.text = " Jawa Timur ";
		
		propinsi.add(option1);
		propinsi.add(option2);
		propinsi.add(option3);
	}
}

function addKota(){
	
	var propinsi = document.getElementById("propinsi");
	var td_Jabar = document.getElementById("td_Jabar");
	var td_Jateng = document.getElementById("td_Jateng");
	var td_Jatim = document.getElementById("td_Jatim");
	
	if(propinsi.value=="Jawa Barat"){
		td_Jabar.style.display="block";
		td_Jateng.style.display="none";
		td_Jatim.style.display="none";
	} 
	else if(propinsi.value=="Jawa Tengah"){
		td_Jateng.style.display="block";
		td_Jabar.style.dislay="none";
		td_Jatim.style.display="none";
	} 
	else if(propinsi.value=="Jawa Timur"){
		td_Jatim.style.display="block";
		td_Jateng.style.display="none";
		td_Jabar.style.dislay="none";
	} 
}
</script>
</html>