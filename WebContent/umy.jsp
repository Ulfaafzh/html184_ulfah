<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Fomulir Penmaru</title>
</head>
<body>
	
	<h1 style="font-size: 40px;" >Fomulir Pendaftaran UMY</h1>
	<table>
		<tr> 
			<td>Nama :</td>
			<td style="color: red; font-size: 12px; display: none" id="td_nama">Please fill out this field</td>
		</tr>
		<tr>
			<td><input type="text" size="125" id="nama"> </td>
		</tr>
		
		
		<tr> 
			<td>No KTP/NIK :</td>
			<td style="color: red; font-size: 12px; display: none" id="td_noktp">Please fill out this field</td>
		</tr>
		<tr>
			<td><input type="text" size="125" id="noktp"> </td>
		</tr>
		
		
		<tr>
			<td> Jenis Kelamin</td>
		</tr>
		<tr>
			<td><input type="radio" name="gender" id="jkPria"> Laki- Laki</td>
		</tr>
		<tr>
			<td><input type="radio" name="gender" id="jkWanita"> Perempuan</td>
		</tr>
		
		
		<tr>
			<td> Agama: </td>
		</tr>
		<tr>
			<td><input type="radio" name="agama" id="islam">Islam</td>
		</tr>
		<tr>
			<td><input type="radio" name="agama" id="kristen">Kristen</td>
		</tr>
		<tr>
			<td><input type="radio" name="agama" id="Katolik">Katolik</td>
		</tr>
		<tr>
			<td><input type="radio" name="agama" id="Hindu">Hindu</td>
		</tr>
		<tr>
			<td><input type="radio" name="agama" id="Budha">Budha</td>
		</tr>
		
		<tr> 
			<td>Tanggal Lahir:</td>
			<td style="color: red; font-size: 12px; display: none" id="td_tglLahir">Please fill out this field</td>
		</tr>
		<tr>
			<td><input type="date" size="125" id="tgl_lahir"> </td>
		</tr>
		
		
		<tr> 
			<td>Tempat Lahir:</td>
			<td style="color: red; font-size: 12px; display: none" id="td_tl">Please fill out this field</td>
		</tr>
		<tr>
			<td><input type="text" size="125" id="tl"> </td>
		</tr>
		
		<tr>
			<td>Negara Lahir</td>
		</tr>
		<tr>
			<td><select id="negaralahir">
					<option value="-">Pilih Negara</option>
					<option value="Indonesia">Indonesia</option>
					<option value="Malaysia">Malaysia</option>
					<option value="Singapur">Singapura</option>
				</select> 
			</td>
			<td style="color: red; font-size: 12px; display: none" id="td_negaralahir">Please select this field</td>
		</tr>
		
		
		<tr>
			<td>Provinsi Lahir</td>
		</tr>
		<tr>
			<td><select id="provinsilahir">
					<option value="-">Select Provinsi</option>
					<option value="DIA">Daerah Istimewa Aceh</option>
					<option value="Sumatera utara">Sumatera Utara</option>
					<option value="Sumatera Barat">Sumatera Barat</option>
				</select> 
			</td>
			<td style="color: red; font-size: 12px; display: none" id="td_provinsilahir">Please select this field</td>
		</tr>
		
		
		<tr>
			<td>Kabupaten Lahir</td>
		</tr>
		<tr>
			<td><select id="kabupatenlahir">
					<option value="-">Select Kabupaten Lahir</option>
				</select> 
			</td>
			<td style="color: red; font-size: 12px; display: none" id="td_kabupatenlahir">Please select this field</td>
		</tr>
		
		
		<tr> 
			<td>Alamat Asal:</td>
			<td style="color: red; font-size: 12px; display: none" id="td_alamatasal">Please fill out this field</td>
		</tr>
		<tr>
			<td><input type="text" size="125" id="alamatasal"> </td>
		</tr>
		
		
		<tr>
			<td>Negara Asal</td>
		</tr>
		<tr>
			<td><select id="negaraAsal">
					<option value="-">Pilih Negara</option>
					<option value="Indonesia">Indonesia</option>
					<option value="Malaysia">Malaysia</option>
					<option value="Singapur">Singapura</option>
				</select> 
			</td>
			<td style="color: red; font-size: 12px; display: none" id="td_negaraAsal">Please select this field</td>
		</tr>
		
		
		<tr>
			<td>Provinsi Asal:</td>
		</tr>
		<tr>
			<td><select id="provinsiasal">
					<option value="-">Select Provinsi</option>
					<option value="DIA">Daerah Istimewa Aceh</option>
					<option value="Sumatera utara">Sumatera Utara</option>
					<option value="Sumatera Barat">Sumatera Barat</option>
				</select> 
			</td>
			<td style="color: red; font-size: 12px; display: none" id="td_provinsiasal">Please select this field</td>
		</tr>
		
		
		<tr>
			<td>Kabupaten Asal:</td>
		</tr>
		<tr>
			<td><select id="kabupatenasal">
					<option value="-">Select Kabupaten Lahir</option>
				</select> 
			</td>
			<td style="color: red; font-size: 12px; display: none" id="td_kabupatenasal">Please select this field</td>
		</tr>
		
		
		<tr>
			<td>Kecamatan Asal:</td>
		</tr>
		<tr>
			<td><select id="kecasal">
					<option value="-">Select Kecamatan Lahir</option>
				</select> 
			</td>
			<td style="color: red; font-size: 12px; display: none" id="td_kecasal">Please select this field</td>
		</tr>
		
		
		<tr> 
			<td>Kelurahan Asal:</td>
			<td style="color: red; font-size: 12px; display: none" id="td_kel">Please fill out this field</td>
		</tr>
		<tr>
			<td><input type="text" size="125" id="kel"> </td>
		</tr>
		
		
		<tr> 
			<td>Kode Pos Asal:</td>
			<td style="color: red; font-size: 12px; display: none" id="td_kodepos">Please fill out this field</td>
		</tr>
		<tr>
			<td><input type="text" size="125" id="kodepos"> </td>
		</tr>
		
		
		<tr> 
			<td>Email:</td>
			<td style="color: red; font-size: 12px; display: none" id="td_email">Please fill out this field</td>
		</tr>
		<tr>
			<td><input type="text" size="125" id="email"> </td>
		</tr>
		
		
		<tr> 
			<td>No Handphone:</td>
			<td style="color: red; font-size: 12px; display: none" id="td_hp">Please fill out this field</td>
		</tr>
		<tr>
			<td><input type="text" size="125" id="hp"> </td>
		</tr>
		
		
		<tr>
			<td>IPA/IPS/IPC</td>
		</tr>
		<tr>
			<td><select id = "jurusanprodi" onclick="pilihJurusan();">
					<option value ="-"> IPA/IPS/IPC </option>
					<option value ="IPA"> Kelompok IPA </option>
					<option value ="IPS"> Kelompok IPS </option>
					<option value ="IPC"> Kelompok IPC </option>
				</select> 
			</td>
			<td style="color: red; font-size: 12px; display: none" id="td_jurusanprodi">Please fill out this field</td>
		</tr>
		
		
		<tr> 
			<td>Harga:</td>
		</tr>
		<tr>
			<td><input type="text" size="125" id="harga"> </td>
		</tr>
		
		
		<tr>
			<td>Prodi Pilihan 1:</td>
		</tr>
		<tr>
			<td><select id="prodi1">
					<option>S1 Teknik Sipil</option>
					<option>S1 Teknik Elektro</option>
					<option>S1 Teknik Mesin</option>
					<option>S1 Teknik Informatika</option>
				</select> 
			</td>
		</tr>
		
		
		<tr>
			<td>Kelas Program Pilihan 1:</td>
		</tr>
		<tr>
			<td><select id="kelas1">
					<option value="Reguler">Reguler</option>
				</select> 
			</td>
		</tr>
		
		
		<tr>
			<td>Prodi Pilihan 2:</td>
		</tr>
		<tr>
			<td><select id="prodi2">
					<option>S1 Teknik Sipil</option>
					<option>S1 Teknik Elektro</option>
					<option>S1 Teknik Mesin</option>
					<option>S1 Teknik Informatika</option>
				</select> 
			</td>
		</tr>
		
		
		<tr>
			<td>Kelas Program Pilihan 2:</td>
		</tr>
		<tr>
			<td><select id="kelas2">
					<option value="Reguler">Reguler</option>
				</select> 
			</td>
		</tr>
		
		
		<tr>
			<td>Negara Asal SLTA:</td>
		</tr>
		<tr>
			<td><select id="negaraAsalslta">
					<option value="-">Pilih Negara</option>
					<option value="Indonesia">Indonesia</option>
					<option value="Malaysia">Malaysia</option>
					<option value="Singapur">Singapura</option>
				</select> 
			</td>
			<td style="color: red; font-size: 12px; display: none" id="td_negaraAsalslta">Please select this field</td>
		</tr>
		
		
		<tr>
			<td><button type="button" >Reset</button> </td>
		</tr>
		
		
		<tr>
			<td>Nama SLTA:</td>
		</tr>
		<tr>
			<td><input type="text" id="namaslta" size="125"> </td>
		</tr>
		
		
		<tr>
			<td>Alamat Asal SLTA:</td>
		</tr>
		<tr>
			<td><input type="text" id="alamatslta" size="125"> </td>
		</tr>
		
		<tr>
			<td>Negara Asal SLTA:</td>
		</tr>
		<tr>
			<td><input type="text" id="negaraslta" size="125"> </td>
		</tr>
		
		<tr>
			<td>Provinsi Asal SLTA:</td>
		</tr>
		<tr>
			<td><input type="text" id="provasal" size="125"> </td>
		</tr>
		
		<tr>
			<td>Kabupaten/Kota Asal SLTA:</td>
		</tr>
		<tr>
			<td><input type="text" id="kabupatenslta" size="125"> </td>
		</tr>
		
		
		<tr>
			<td>Jurusan SLTA</td>
		</tr>
		<tr>
			<td><input type="text" id="jurusanslta" size="125"> </td>
		</tr>
		
		
		<tr>
			<td><button type="button" onclick="cekIsi()">Simpan</button> </td>
		</tr>
		
	</table>

</body>
<script type="text/javascript">
	function cekIsi() {
		var nama= document.getElementById("nama");
		var td_nama= document.getElementById("td_nama");
		if (nama.value=="") {
			td_nama.style.display="block";
			nama.style.borderColor ="red";
		}else {
			td_nama.style.display="none";
		}
		
		var noktp= document.getElementById("noktp");
		var td_noktp= document.getElementById("td_noktp");
		if (noktp.value=="") {
			td_noktp.style.display="block";
			noktp.style.borderColor ="red";
		}else {
			td_noktp.style.display="none";
		}
		
		var tgl_lahir= document.getElementById("tgl_lahir");
		var td_tglLahir= document.getElementById("td_tglLahir");
		if (tgl_lahir.value=="") {
			td_tglLahir.style.display="block";
			tgl_lahir.style.borderColor ="red";
		}else {
			td_tglLahir.style.display="none";
		}
		
		var tl= document.getElementById("tl");
		var td_tl= document.getElementById("td_tl");
		if (tl.value=="") {
			td_tl.style.display="block";
			tl.style.borderColor ="red";
		}else {
			td_tl.style.display="none";
		}
		
		var negaralahir= document.getElementById("negaralahir");
		var td_negaralahir= document.getElementById("td_negaralahir");
		if (negaralahir.value=="-") {
			td_negaralahir.style.display="block";
			negaralahir.style.borderColor ="red";
		}else {
			td_negaralahir.style.display="none";
		}
		
		var provinsilahir= document.getElementById("provinsilahir");
		var td_provinsilahir= document.getElementById("td_provinsilahir");
		if (negaralahir.value=="-") {
			td_provinsilahir.style.display="block";
			provinsilahir.style.borderColor ="red";
		}else {
			td_provinsilahir.style.display="none";
		}
		
		var kabupatenlahir= document.getElementById("kabupatenlahir");
		var td_kabupatenlahir= document.getElementById("td_kabupatenlahir");
		if (kabupatenlahir.value=="-") {
			td_kabupatenlahir.style.display="block";
			kabupatenlahir.style.borderColor ="red";
		}else {
			td_kabupatenlahir.style.display="none";
		}
		
		var alamatasal= document.getElementById("alamatasal");
		var td_alamatasal= document.getElementById("td_alamatasal");
		if (alamatasal.value=="") {
			td_alamatasal.style.display="block";
			alamatasal.style.borderColor ="red";
		}else {
			td_alamatasal.style.display="none";
		}
		
		var kel= document.getElementById("kel");
		var td_kel= document.getElementById("td_kel");
		if (kel.value=="") {
			td_kel.style.display="block";
			kel.style.borderColor ="red";
		}else {
			td_kel.style.display="none";
		}
		
		var kodepos= document.getElementById("kodepos");
		var td_kodepos= document.getElementById("td_kodepos");
		if (kodepos.value=="") {
			td_kodepos.style.display="block";
			kodepos.style.borderColor ="red";
		}else {
			td_kodepos.style.display="none";
		}
		
		var email= document.getElementById("email");
		var td_email= document.getElementById("td_email");
		if (email.value=="") {
			td_email.style.display="block";
			email.style.borderColor ="red";
		}else {
			td_email.style.display="none";
		}
		
		var hp= document.getElementById("hp");
		var td_hp= document.getElementById("td_hp");
		if (hp.value=="") {
			td_hp.style.display="block";
			hp.style.borderColor ="red";
		}else {
			td_hp.style.display="none";
		}
		
		var jurusan= document.getElementById("jurusan");
		var td_jurusan= document.getElementById("td_jurusan");
		if (jurusan.value=="-") {
			td_jurusan.style.display="block";
			jurusan.style.borderColor ="red";
		}else {
			td_jurusan.style.display="none";
		}
		
		var negaraAsalslta= document.getElementById("negaraAsalslta");
		var td_negaraAsalslta= document.getElementById("td_negaraAsalslta");
		if (negaraAsalslta.value=="-") {
			td_negaraAsalslta.style.display="block";
			negaraAsalslta.style.borderColor ="red";
		}else {
			td_negaraAsalslta.style.display="none";
		}
	}
	
	function pilihJurusan() {
		var jurusan = document.getElementById("jurusan");
		var harga = document.getElementById("harga");
		
			jurusan.value=harga.value;
	}
	

</script>
</html>