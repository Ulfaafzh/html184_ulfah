<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<form action="#">
		<table>
			<tr>
				<td> NAMA </td>
				<td><input type="text" id="nama"></td>
				<td style="color: red; display: none;" id="td_nama">nama kosong!!!</td>
			</tr>
			<tr>
				<td> PASSWORD </td>
				<td><input type="password" id="password"></td>
				<td style="color: red; display: none;" id="td_password">password kosong!!!</td>
			</tr>
			<tr>
				<td> REPASSWORD </td>
				<td><input type="password" id="repassword" onchange="cekSama();"></td>
				<td style="color: red; display: none;" id="td_repassword">password tidak sama!!!</td>
			</tr>
			<tr>
				<td>
					<select id="provider" onclick="pilihNomor();">
						<option value="-"> Pilih </option>
						<option value="0812"> Telkomsel </option>
						<option value="0856"> Indosat </option>
						<option value="0878"> XL </option>
						<option value="0896"> 3 </option>
						<option value="0888"> Smartfren </option>
					</select>
				</td>
				<td> <input type="text" id="hp" onkeyup="pilihProvider();"/> </td>
			</tr>
			<tr>
				<td> <button type="reset">Reset</button> </td>
				<td> <button type="button" onclick="cekIsi();"> Submit</button> </td>
			</tr>
		</table>
	</form>
</body>
<script type="text/javascript">
	function cekIsi() {
		var nama= document.getElementById("nama");
		var td_nama= document.getElementById("td_nama");
		if (nama.value=="") {
			td_nama.style.display="block";
			nama.style.borderColor ="red";
		}else {
			td_nama.style.display="none";
			nama.style.borderColor ="gray";
		}
		
		var password= document.getElementById("password");
		var td_password= document.getElementById("td_password");
		if (password.value=="") {
			td_password.style.display="block";
			password.style.borderColor ="red";
		}else {
			td_password.style.display="none";
			password.style.borderColor ="gray";	
			
		}
	}
	
	function cekSama() {
		var password=document.getElementById("password");
		var repassword=document.getElementById("repassword");
		var td_repassword=document.getElementById("td_repassword");
		if (password.value!=repassword.value) {
			td_repassword.style.display="block";
		} else {
			td_repassword.style.display="none";
		}
	}
	
	function pilihNomor() {
		var provider = document.getElementById("provider");
		var hp = document.getElementById("hp");
		
		hp.value=provider.value;
	}
	
	function pilihProvider() {
		var provider = document.getElementById("provider");
		var hp = document.getElementById("hp");
		
		provider.value=hp.value;
	}
</script>

</html>
