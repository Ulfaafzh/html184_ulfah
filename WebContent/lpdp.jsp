<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Beasiswa LPDP - Create User</title>
</head>
<body>
	<h2 style="font: Tahoma">Daftarkan Akun</h2>
	<a style="color: red">Catatan :semua isian bertanda * perlu diisi</a>
	
	<form action="">
		<br>
		<h3>INFORMASI AKUN</h3>
		<table>
			<tr>
				<td></td>
				<td style="color: red; display: none;"id="td_email">Data Akun Atas Nama Anda Sudah Terdaftar</td>
			</tr>
			<tr> 
				<td>Email<a style="color: red">*</a></td>
				<td><input type="text" size="75" id="email"></td> 
			</tr>
			<tr>
				<td></td>
				<td style="color: red; display: none;"  id="td_reemail">Email Berbeda</td>
			</tr>
			<tr> 
				<td>Konfirmasi Email </td>
				<td><input type="text" size="75" id="reemail" onchange="cekEmail()"></td> 
			</tr>
			<tr> 
				<td>Password<a style="color: red">*</a></td>
				<td><input type="password" size="75" id="password"></td> 
			</tr>
			<tr>
				<td></td>
				<td style="color: red; display: none;"  id="td_repassword">Password Berbeda</td>
			</tr>
			<tr> 
				<td>Konfirmasi Password<a style="color: red">*</a></td>
				<td><input type="password" size="75" id="repassword" onchange="cekPassword()"></td> 
			</tr>
			
		</table>
		
		
		
		<br>
		<h3>INFORMASI PRIBADI</h3>
		<table>
			<tr> 
				<td>Nama lengkap<a style="color: red">*</a></td> 
			</tr>
			<tr>
				<td><a style="color: red; size: 12px;">Sesuai KTP dan tanpa <br> 
			    gelar. Penggunaan<br>huruf kapital hanya <br> diperkenankan diawal <br>
			    kata (contoh : Ahmad<br> Budiawan)</a></td>
			    <td><input type="text" size="75" id="nama"></td>
			</tr>
			<tr> 
				<td>Nama Panggilan<a style="color: red">*</a></td>
				<td><input type="text" size="75" id="napang"></td> 
			</tr>
			<tr> 
				<td>No KTP<a style="color: red">*</a></td>
				<td><input type="text" size="75" id="noktp"></td> 
			</tr>
			<tr> 
				<td>Jenis Kelamin<a style="color: red">*</a></td>
				<td><select id="jk">
						<option style="size: 75" value="Laki-Laki"> Laki-Laki </option>
						<option style="size: 75" value="Perempuan"> Perempuan </option>
					</select>
				</td> 
			</tr>
			<tr> 
				<td>Status Menikah<a style="color: red">*</a></td>
				<td><select id="status">
						<option style="size: 75" value="Belum Menikah"> Belum Menikah </option>
						<option style="size: 75" value="Menikah"> Menikah </option>
						<option style="size: 75" value="Cerai"> Duda/Janda/Cerai </option>
					</select>
				</td> 
			</tr>
			<tr> 
				<td>Agama<a style="color: red">*</a></td>
				<td><select id="agama">
						<option style="size: 75" value="Islam"> Islam </option>
						<option style="size: 75" value="Protestan"> Protestan </option>
						<option style="size: 75" value="Katolik"> Katolik </option>
						<option style="size: 75" value="Hindu"> Hindu </option>
						<option style="size: 75" value="Budha"> Budha </option>
						<option style="size: 75" value="Kong Hu Cu"> Kong Hu Cu </option>
					</select>
				</td> 
			</tr>
			<tr> 
				<td>Golongan Darah<a style="color: red">*</a></td>
				<td><select id="gol">
						<option style="size: 75" value="A"> A </option>
						<option style="size: 75" value="B"> B </option>
						<option style="size: 75" value="AB"> AB </option>
						<option style="size: 75" value="O"> O </option>
					</select>
				</td> 
			</tr>
			<tr>
				<td>Tempat Lahir<a style="color: red;">*</a></td>
				<td><input type="text" size="75" id="tl"></td>
			</tr>
			<tr>
				<td>Tanggal Lahir<a style="color: red;">* <br> Sesuai KTP</a></td>
				<td><input type="date" size="75" id="tglahir"></td>
			</tr>
			<tr>
			<tr>
				<td>Alamat<a style="color: red;">* <br> Sesuai KTP</a></td>
				<td><input type="text" size="75" id="alamat"></td>
			</tr>
			<tr> 
				<td>Kabupaten/Kota<a style="color: red">*</a></td>
				<td><select id="kota">
						<option style="size: 75" value="DKI Jakarta"> DKI Jakarta </option>
						<option style="size: 75" value="Kota Bogor"> Kota Bogor </option>
						<option style="size: 75" value="Kota Depok"> Kota Depok </option>
						<option style="size: 75" value="Kota Bekasi"> Kota Bekasi </option>
					</select>
				</td> 
			</tr>
			<tr>
				<td>Kode Pos<a style="color: red;">* <br></a></td>
				<td><input type="text" size="75" id="kodepos"></td>
			</tr>
			<tr> 
				<td>Handphone<a style="color: red">*</a></td> 
			</tr>
			<tr>
				<td><a style="color: red; size: 12px;">Harap diisi dengan <br> 
			    format menggunakan<br>kode negara.Untuk<br>Indonesia, gantikan<br>
			    angka 0 di paling<br> depan dengan angka<br> 62. Contoh: <br>
			    081111111 mohon <br>diisi dengan<br>6281111111 </a></td>
			    <td><input type="text" size="75" id="hp"></td>
			</tr>
			<tr>
				<td>Telepon</td>
				<td><input type="text" size="75" id="telp"></td>
			</tr>
			<tr> 
				<td>Jenis Pekerjaan<a style="color: red">*</a></td>
				<td><select id="jp">
						<option style="size: 75" value="Ahli Profesional"> Ahli Professional </option>
						<option style="size: 75" value="Bidan/Perawat"> Bidan/Perawat </option>
						<option style="size: 75" value="Dokter"> Dokter </option>
						<option style="size: 75" value="Dosen"> Dosen </option>
					</select>
				</td> 
			</tr>
			<tr>
				<td>Foto (4x6)</td>
				<td><input type="file" size="75" id="foto"></td>
			</tr> 
		</table>
		
		<br>
		<h3>PENDIDIKAN TERAKHIR</h3>
		
		<table>
			<tr> 
				<td>Jenjang Pendidikan<a style="color: red">*</a></td>
				<td><select id="pendidikan">
						<option style="size: 75" value="S1/D4"> S1/D4 </option>
						<option style="size: 75" value="S2"> S2 </option>
						<option style="size: 75" value="S3"> S3 </option>
					</select>
				</td> 
			</tr>
			<tr> 
				<td>Tahun Kelulusan<a style="color: red">*</a></td>
				<td><select id="tahunlulus">
						<option value="1995"> 1995 </option>
						<option value="1996"> 1996 </option>
						<option value="1997"> 1997 </option>
						<option value="1998"> 1998 </option>
						<option value="1999"> 1999 </option>
						<option value="2000"> 2000 </option>
						<option value="2001"> 2001 </option>
						<option value="2002"> 2002 </option>
						<option value="2003"> 2003 </option>
						<option value="2004"> 2004 </option>
						<option value="2005"> 2005 </option>
						<option value="2006"> 2006 </option>
						<option value="2007"> 2007 </option>
						<option value="2008"> 2008 </option>
						<option value="2009"> 2009 </option>
						<option value="2010"> 2010 </option>
						<option value="2011"> 2011 </option>
						<option value="2012"> 2012 </option>
						<option value="2013"> 2013 </option>
						<option value="2014"> 2014 </option>
						<option value="2015"> 2015 </option>
						<option value="2016"> 2016 </option>
						<option value="2017"> 2017 </option>
						<option value="2018"> 2018 </option>
						<option value="2019"> 2019 </option>
					</select>
				</td> 
			</tr>
			<tr> 
				<td>Asal Universitas</td>
				<td><select id="asaluniv">
						<option value="Dalam Negeri"> Dalam Negeri </option>
						<option value="Luar Negeri"> Luar Negeri </option>
					</select>
				</td> 
			</tr>
			<tr> 
				<td>Perguruan Tinggi<a style="color: red">*</a></td>
				<td><select id="asaluniv">
						<option> Iisip Yapis Biak </option>
						<option> IKIP Budi Utomo </option>
						<option> IKIP Gunung Sitoli </option>
					</select>
				</td> 
			</tr>
			<tr> 
				<td>Prodi<a style="color: red">*</a></td>
				<td><select id="prodi">
						<option> Administrasi </option>
						<option> Administrasi Asuransi </option>
						<option> Administrasi Bisnis </option>
					</select>
				</td> 
			</tr>
			<tr>
				<td>IPK Terakhir dengan<br> Format 4.00<a style="color: red">*</a></td>
				<td><input type="text" size="75" id="ipk"> </td>
			</tr>
			<tr>
				<td></td>
				<td><u href="https://www.scholaro.com/gpa-calculator/" style="color: blue;">Link Konversi IPK Luar Negeri</u></td>
			</tr>
			<tr>
				<td></td>
				<td> <button type="button" onclick="cekIsi();"> Buat Akun</button> </td>
			</tr>
		</table>
		
	</form>
	
</body>
<script type="text/javascript">
	function cekIsi(){
		var email= document.ElementById("email");
		var td_email = document.ElementById("td_email");
		
		if(email.value==""){
			td_email.style.display="block";
		}else{
			td_email.style.display="none";
		}
	}
	
	function cekEmail(){
		var reemail = document.getElementById("reemail");
		var td_reemail= document.getElementById("td_reemail");
		if (reemail.value!=email.value) {
			td_reemail.style.display="block";
		} else {
			td_reemail.style.display="none";
		}
	}

	function cekPassword(){
		var repassword = document.getElementById("repassword");
		var td_repassword= document.getElementById("td_repassword");
		if (repassword.value!=password.value) {
			td_repassword.style.display="block";
		} else {
			td_repassword.style.display="none";
		}
	}
</script>
</html>